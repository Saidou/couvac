---
output: pdf_document
header-includes:
  - \usepackage{booktabs}
  - \usepackage{placeins}
  - \usepackage{float}
  - \usepackage{tabu}
---
\setlength{\unitlength}{1cm}

\begin{picture}(0,0)(-12.5, -1)
\includegraphics[height = 1.1 cm]{logo/LogoCermel.png}
\end{picture}

\begin{center}
\Large{\textbf{Statistical analysis report of COUVAC}}
\end{center}

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE)
options(knitr.kable.NA = "-")
library(knitr)
library(kableExtra)
```


```{r}
tab_1 %>%
  select(-names) %>% 
  kable(caption = "Baseline Characteristics of Participants",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "N(%)"), align = c("l", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position"), font_size = 8) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows(" ", 1, 1) %>%
  pack_rows("Person Interviewed (PI)", 2, 4) %>%
  pack_rows("Age PI", 5, 9) %>%
  pack_rows("Marital status PI", 10, 14) %>%
  pack_rows("Study level PI", 15, 19) %>%
  pack_rows("Profession PI", 20, 25) %>%
  pack_rows("Gender", 26, 28) %>%
  pack_rows("Age Group", 29, 30) %>%
  pack_rows("Locality", 31, 33) %>%
  pack_rows("Religion", 34, 38) %>%
  pack_rows("presence of a health record", 39, 40) %>%
  column_spec(1, width = "5cm")
```


```{r}
tab_2 %>%
  select(-type) %>% 
  kable(caption = "Prevalence of immunization coverage",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", " All", "With health book", "Without health book"),
        align = c("l", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>% 
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows("Vaccine coverage before one year", 1, 2) %>%
  pack_rows("Coverage of boosters", 3, 4) %>%
  pack_rows("Vaccine coverage", 5, 6) %>%
  column_spec(1, width = "5cm")
```

```{r}
tab_3 %>%
  kable(caption = "Distribution of each type of vaccine",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", " All", "With health book", "Without health book"),
        align = c("l", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>% 
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows("Vaccine coverage before one year", 1, 10) %>%
  pack_rows("Coverage of boosters", 11, 13) %>%
  column_spec(1, width = "5cm")
```


```{r}
tab_4 %>%
  kable(caption = "Reasons why the child did not receive the recommended vaccines",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "N (%)"),
        align = c("l", "r"),
        format.args = list(big.mark = ' ')) %>% 
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE)
```

```{r}
fig_1
```

```{r}
fig_2
```

